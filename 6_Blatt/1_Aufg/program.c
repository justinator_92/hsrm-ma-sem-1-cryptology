#include <stdlib.h>
#include <stdio.h>

#include "../../cryptlib/cryptlib.h"
#include "../../cryptlib/utils.h"


char * encrypt(char * plaintext, unsigned long long t, unsigned long long k, unsigned long long n) {

    char * handler = plaintext;
    char c, a;

    while(*handler) {
        c = toLower(*handler) - 'a';
        a = ((char) affine(c, t, k, n));
        *handler++ = a + 'a';
    }

    return plaintext;
}

char * decrypt(char * ciphertext, unsigned long long t, unsigned long long k, unsigned long long n) {

    unsigned long long inv = ModInvS12(t, n);

    char * handler = ciphertext;
    char c, a;

    while(*handler) {
        c = toLower(*handler) - 'a';
        unsigned long long re = affineInv(c, t, k, n, inv);
        a = (char) re;
        *handler++ = a + 'a';
    }

   

    return ciphertext;
}


int main(void) {

    char c[] = "diesisteinneuertext";
    char * content = c;

    content = encrypt(content, 5, 7, 26);
    printf("ciphertext:\t%s\n", content);
    content = decrypt(content, 5, 7, 26);
    printf("plaintext:\t%s\n", content);

    content = encrypt(content, 17, 4, 256);
    printf("ciphertext:\t%s\n", content);
    content = decrypt(content, 17, 4, 256);
    printf("ciphertext:\t%s\n", content);

    return 0;
} 