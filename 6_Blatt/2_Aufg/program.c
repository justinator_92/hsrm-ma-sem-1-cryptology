#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "../../cryptlib/3x3PriorityMatrix.h"
#include "../../cryptlib/cryptlib.h"

Matrix * encrypt(Matrix * key, Matrix * plainText, unsigned long long n) {

    Matrix * ret = key->mult_m(key, plainText);
    ret->mod(ret, n);
    return ret;
}


Matrix * decrypt(Matrix * key, Matrix * cipherText, unsigned long long n) {

    unsigned long long det = key->det(key, n);
    assert(det != 0);               // first condition that inverse exists...
    assert(ggTS12(det, n) == 1);    // second condition that inverse exists...

    unsigned long long inv = ModInvS12(det, n);
    
    Matrix * adj = key->adj(key, n);
    Matrix * i = adj->mult_v(adj, inv);
    i->mod(i, n);
    
    Matrix * r = i->mult_m(i, cipherText);
    r->mod(r, n);
    return r;
}


int main(void) {

    unsigned long long k[] = { 6, 24, 1, 13, 16, 10, 20, 17, 15 };
    unsigned long long a[] = { 0, 1, 1, 2, 1, 1, 19, 1, 1 };
    Matrix * ma = create_Matrix(a);
    Matrix * mk = create_Matrix(k);

    ma->repr(ma, "ma");
    mk->repr(mk, "mk");

    Matrix * c = encrypt(mk, ma, 26);
    c->repr(c, "ciphertext");

    Matrix * p = decrypt(mk, c, 26);
    p->repr(p, "plaintext");

    destroy_Matrix(ma);
    destroy_Matrix(mk);
    destroy_Matrix(c);
    destroy_Matrix(p);

    return 0;
} 