#!/usr/bin/python

# A python crypt library build for cryptologic leture in 



def eeA(a, b):
	if (b == 0): return (a, 1, 0)
	(d, x, y) = eeA(b, a % b)
	return (d, y, x - (a / b) * y)
	
def findPrimeFactors(n):
	primes = [x for x in range(2, n) if [y for y in range(2, x) if x%y == 0] == []]

	for p in primes:
		for q in primes:
			if p * q == n:
				return p, q
	return None


if __name__ == '__main__':
	#print findPrimeFactors(13439)

	key1 = [1, 0, 1, 0, 1, 0, 1, 0]
	key2 = [1, 1, 0, 0, 1, 1, 0, 0]
	key3 = [0, 0, 1, 1, 0, 0, 1, 1]
	
	m1 = [1, 0, 0, 1]
	m2 = [0, 1, 1, 0]
	m3 = [1, 1, 0, 0]
	m4 = [0, 0, 1, 1]

	block = Block([key1, key2, key3], [m1, m2, m3, m4], )
