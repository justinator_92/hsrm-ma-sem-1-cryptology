#ifndef _MATRIX_H
#define _MATRIX_H

/**
 *	This is a matrix header file that provides basic
 * 	factionality like calculating determinants
 * 	inverting
 * 	
 * 	@author: Justin Albert
 *
 */


/**
 *	struct definition for matrix
 */
struct _matrix {

	unsigned long long * values;

	void (*repr)(struct _matrix *, char * name);
	unsigned long long (*det)(struct _matrix *, unsigned long long n);
	struct _matrix * (*mult_v)(struct _matrix *, unsigned long long value);
	struct _matrix * (*mult_m)(struct _matrix *, struct _matrix *);
	void (*mod)(struct _matrix *, unsigned long long n);
	struct _matrix * (*trans)(struct _matrix *);
	struct _matrix * (*adj)(struct _matrix *, unsigned long long);

} typedef Matrix;


/*
 *	helper method to get linear index for two dimensions...
 */
int helper_Idx(int x, int y, int cols);

/*
 *	console representation for matrix m
 *	printed out
 */
void print_Matrix(Matrix * m, char * name);


/**
 *	calculate determinant of Matrix m
 * 	uses rule of sarrus, so it's only working for A1,1 A2,2 and A3,3
 */
unsigned long long get_Det(Matrix * m, unsigned long long n);


/**
 *	multiplies a value to an entire matrix cell by cell
 */
Matrix * mul_matrix_v(Matrix * m, unsigned long long value);

/**
 *	multiplies matrix to a matrix. They are not commutative
 */
Matrix * mul_matrix_m(Matrix * m1, Matrix * m2);


/**
 *	transpose a matrix
 */
Matrix * transpose(Matrix * m);


/**
 *	get adjungate matrix and mod n to entire matrix cell by cell
 */
Matrix * adjungate(Matrix * m, unsigned long long n);


/**
 *	calculate mod to entire matrix cell by cell
 */
void module(Matrix * m, unsigned long long n);


/**
 *	constructor function for matrix
 */
Matrix * create_Matrix(unsigned long long * values);


/**
 *	release allocated memory
 */
void destroy_Matrix(Matrix * m);

#endif