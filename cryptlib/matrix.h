#ifndef _MATRIX_H
#define _MATRIX_H

/**
 *	This is a matrix header file that provides basic
 * 	factionality like calculating determinants
 * 	inverting
 * 	
 * 	@author: Justin Albert
 *
 */

struct _matrix {

	int cols, rows;
	int * values;

	void (*repr)(struct _matrix *);
	int (*det)(struct _matrix *);
	struct _matrix * (*mult_v)(struct _matrix *, int value);
	struct _matrix * (*mult_m)(struct _matrix *, struct _matrix *);
	void (*mod)(struct _matrix *, int n);
	struct _matrix * (*trans)(struct _matrix *);
	struct _matrix * (*inv)(struct _matrix *, int n);
	struct _matrix * (*adj)(struct _matrix *);
	struct _matrix * (*inv2)(struct _matrix *, int n);
	struct _matrix * (*subM)(struct _matrix *, int c, int r);


} typedef Matrix;


/*
 *	helper method to get linear index for two dimensions...
 */
int helper_Idx(int x, int y, int cols);

/*
 *	console representation for matrix m
 *	printed out
 */
void print_Matrix(Matrix * m);


/**
 *	calculate determinant of Matrix m
 * 	uses rule of sarrus, so it's only working for A1,1 A2,2 and A3,3
 */
int get_Det(Matrix * m);

Matrix * mul_matrix_v(Matrix * m, int value);

Matrix * mul_matrix_m(Matrix * m1, Matrix * m2);

Matrix * invert(Matrix * m, int n);
Matrix * invert2(Matrix * m, int n);

Matrix * transpose(Matrix * m);

Matrix * adjungate(Matrix * m);

void module(Matrix * m, int n);

Matrix * create_Matrix(int col, int row, int * values);

void destroy_Matrix(Matrix * m);

#endif