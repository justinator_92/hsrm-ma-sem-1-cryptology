#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "cryptlib.h"
#include "utils.h"
#include "matrix.h"


/**
 *	Berechnet die Lösung der Gleichung ggT(a, b) = a*x + b*y
 */
unsigned long long eEAS12(unsigned long long a, unsigned long long b, long long* x, long long* y) {

	if(b == 0) {
		*x = 1;
		*y = 0;
		return a;
	}
		
	unsigned long long d = eEAS12(b, ModS12(a, b), x, y);

	long long h = *x;
	*x = *y;
	*y = h - ((long long) (a / b)) * *y;

	return d;
}


/*
 *	Berechnet Lösung der Diophantischen Gleichung d = a*x + b*y
 */
unsigned long long eEAdS12(unsigned long long a, unsigned long long b, unsigned long long d, long long* x, long long* y) {

	unsigned long long ggT = ggTS12(a, b);

	if(ModS12(d, ggT) != 0) // es existiert keine Lösung...
		return -1;

	unsigned long long nD = DivS12(d, ggT);

	eEAS12(DivS12(a, ggT), DivS12(b, ggT), x, y);
	*x = *x * nD;
	*y = *y * nD;
	return ggT;
}


/*
 *	liefert den Wert der n-ten Primzahl
 */
unsigned long long prime1S12(unsigned long long n) {

	int l = 2;
	int i = 0;

	while(1) {
		if(isPrimeS12(l) != -1) {
			i += 1;
		}

		if(i >= n)
			return l;

		l += 1;
	}

}

/**
 *	liefert den Wert einer zufällig im Intervall [a, b] gewählten Primzahl
 */
unsigned long long primeZS12 (unsigned long long a, unsigned long long b) {
	
	int *array;
	array = malloc(sizeof(unsigned long long) * (b-a));

	unsigned long long i, n = 0;
	
	for(i = a; i < b; i++) 
		if(isPrimeS12(i) == 0) 
			array[n++] = i;



	srand(time(0));
	int r = (int)(0 + (n - 1) * rand() / ((double) RAND_MAX));
	unsigned long long ret = array[r];

	free(array);

	return ret;
}

/**
 *	berechnet die Primzahlen p mit 2<= p <= n. 
 *	Ergebnisübergabe per call-by-reference
 */
unsigned long long primeNS12(unsigned long long* ptr, unsigned long long n) {

	int i;
	unsigned long long counter = 0;
	
	for(i = 2; i < n + 1; i++) {
		if(isPrimeS12(i) == 0) {
			ptr[counter++] = i;
		}
	}

	return counter;
}


/**
 *	liefert den Wert 0 falls n eine Primzahl ist, oder 1 falls n keine Primzahl ist
 */
unsigned int isPrimeS12(unsigned long long n) {

	int i;
	for(i = 2; i < n / 2; i++) 
		if(n % i == 0 ) return 1;
	
	return 0;
}


/**
 *	Ermittelt die Pseudozufallsfolge gemäß für genau einen
 *	Iterationsschritt in Abhängigkeit des Startwertes x
 */
unsigned long long RandS12(unsigned long long a, unsigned long long b, unsigned long long n, unsigned long long x) {

	return ModS12((a * x + b), n);

}


/**
 *	Berechnet Wert des ggT unter Verwendendung des 
 *	euklidschen Algorithmus
 */
unsigned long long ggTS12(unsigned long long a, unsigned long long b) {

	if(b == 0)
		return a;
	return ggTS12(b, ModS12(a, b));

}


/**
 *	Berechnet die eulersche Phi Funktion
 *	Algorithmus benutzt die Primfaktorzerlegung
 */
unsigned long long EulPhiS12(unsigned long long n) {

	if(isPrimeS12(n) == 0) return n - 1;

	unsigned long long i, c = 0;

	for(i = 1; i < n; i++) {
		if(ggTS12(i, n) == 1)
			c++;
	}

	return c;

	/*unsigned long long i, t, c, ret = 1;
	c = n;*/

	/*for(i = 2; i < c; i++) {
		t = 1;
		while((n % i) == 0) {
			//printf("Jepp: %llu\n", i);
			t *= i;  // phi(p) = p-1
			n /= i;
		}

		if(t > 1) {
			printf("Euler funktion (%llu)\n", t);
			unsigned long long e = EulPhiS12(t);
			printf("Result: %llu\n", e);
			ret *= e;
		}
			
	}*/

	//return ret;
}


/**
 *	liefert den ganzzahligen Anteil der Division von a und n
 */
unsigned long long DivS12(long long a, long long n) {
	
	//printf("a: %lld, n: %lld\n", a, n);

	assert(n != 0);

	long long sign = 1;
	//printf("a: %lld \n", a);

	if(a < 0)
		sign *= -1;
		
	if(n < 0)
		sign *= -1;

	unsigned long long ret = (abs(a) + (abs(n) / 2)) / abs(n);
	//printf("ret: %lld, sign: %lld\n", ret, sign);
	return ret * sign;

}


/**
 *	liefert den Rest der Division von a und n
 */
unsigned long long ModS12(unsigned long long a, unsigned long long n) {

	/*if(b < 0)
		return ModS12(-a, -n);
	unsigned long long r = a % n;
	return r < 0 ? r + n : r;*/
	//printf("div: %lld\n", d);
	long long ret = a - DivS12(a, n) * n;
	if(ret < 0)
		ret += n;

	//printf("%lld mod %lld = %lld\n", a, n, ret);

	return ret;

}


/**
 *	liefert die Inverse zu a module n.
 * 	Falls keine Inverse existiert ist Rückgabewert NULL
 */
unsigned long long ModInvS12(unsigned long long a, unsigned long long n) {

	if(ggTS12(a, n) != 1) return 0;

	long long x1 = 0;
	long long y1 = 0;

	long long * x = &x1;
	long long * y = &y1;

	eEAS12(a, n, x, y);
	return x1 >= 0 ? x1 : x1 + n;
}



/**
 *	berechnet die b-te Potenz von a module n. 
 *	d.h. a^b mod n gemäß Square-and-multiply algorithmus
 *	und dem geib'schen Entwicklungssatz
 */
unsigned long long ModExpS12(unsigned long long a, unsigned long long b, unsigned long long n) {

	if(ggTS12(a, n) == 1 && b > n) 
		return ModExpS12(a, b % EulPhiS12(n), n);  // geibscher Entwicklungssatz

	unsigned long long d = 1;

	char * k = tobinstr(b);

	while(*k != '\n') {

		d = ModS12(d * d, n);

		if(*k++ == '1')
			d = ModS12(d * a, n);
		
	}

	return d;
}



/**
 *	Berechnet den Wert der affinen Tauschchiffre
 *	d.h. die funktion berechnet: (p * t + k) Mod n
 *	
 */
unsigned long long affine(int p, unsigned long long t, unsigned long long k, unsigned long long n) {

    assert(ggTS12(t, n) == 1);
    unsigned long long ret = ModS12((p * t + k), n);
  	//printf("%d, %lld, %lld, %lld, %lld\n", p, t, k, n, ret);
    return ret;

}

/**
 *	Berechnet den inversen Wert der Tauschchiffre
 *	Benutzt zur berechnung das modulare inverse von t und n
 *	Macht folgende Berechnung: (c - k * Inv(t, n)) Mod n
 */
unsigned long long affineInv(int c, unsigned long long t, unsigned long long k, unsigned long long n, unsigned long long inv) {

    unsigned long long ret = ModS12((c - k) * inv, n);
 	//printf("%d, %lld, %lld, %lld %lld %lld\n", c, t, k, n, inv, ret);
    return ret;
}