#include "3x3PriorityMatrix.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "cryptlib.h"
#include "utils.h"

/**
 *	This is a matrix class implementation that provides basic factionality
 *	e.g. calculating determinants, inverting, multiplying
 * 	
 * 	@author:	Justin Albert
 *	@date:		19.01.2016
 */


/*
 *	helper method to get linear index for two dimensions...
 */
int helper_Idx(int x, int y, int cols) {
	return y * cols + x;
}


/*
 *	console representation for matrix m
 *	printed out
 */
void print_Matrix(Matrix * m, char * name) {

	int x, y;	
	printf("%s = \n", name);
	for(y = 0; y < 3; y++) {
		printf("%c ", '|');
		for(x = 0; x < 3; x++) {
			printf("%lld ", m->values[helper_Idx(x, y, 3)]);
		}
		printf("%c\n", '|');
	}
	
}


/**
 *	calculate determinant for 3x3 Matrix m using rule of sarrus
 */
unsigned long long get_Det(Matrix * m, unsigned long long n) {
	
	unsigned long long ret = m->values[0] * m->values[4] * m->values[8] +
							 m->values[1] * m->values[5] * m->values[6] +
							 m->values[2] * m->values[3] * m->values[7] -
							 m->values[6] * m->values[4] * m->values[2] -
							 m->values[7] * m->values[5] * m->values[0] -
							 m->values[8] * m->values[3] * m->values[1];
	
	return ModS12(ret, n);
}

/**
 *	multiplies a value to whole matrix cell by cell
 */
Matrix * mul_matrix_v(Matrix * m, unsigned long long value) {

	int i;
	for(i = 0; i < 9; i++)
		m->values[i] = m->values[i] * value;
	return m;
}


/**
 *	multiplies two matrices, be careful, they are not commutative!
 */
Matrix * mul_matrix_m(Matrix * m1, Matrix * m2) {

	unsigned long long r[9];

	int c, d, k = 0;
	unsigned long long sum = 0;

	for (c = 0; c < 3; c++) {
		for (d = 0; d < 3; d++) {
    		for (k = 0; k < 3; k++) {
      			sum = sum + m1->values[helper_Idx(k, c, 3)] * m2->values[helper_Idx(d, k, 3)];
			}
			r[helper_Idx(d, c, 3)] = sum;
    		sum = 0;
    	}
	}
	Matrix * result = create_Matrix(r);

	return result;
}


/**
 *	transpose matrix
 */
Matrix * transpose(Matrix * m) {

	unsigned long long r[9];
	Matrix * result = create_Matrix(r);

	int x, y;

	for (y = 0; y < 3; y++) 
		for (x = 0; x < 3; x++) 
        		result->values[helper_Idx(y, x, 3)] = m->values[helper_Idx(x, y, 3)];

	return result;
}

/**
 *	get adjungate matrix
 */ 
Matrix * adjungate(Matrix * m, unsigned long long n) {

	unsigned long long r[9] = { m->values[4] * m->values[8] - m->values[5] * m->values[7],
								m->values[2] * m->values[7] - m->values[1] * m->values[8],
								m->values[1] * m->values[5] - m->values[2] * m->values[4],
								m->values[5] * m->values[6] - m->values[3] * m->values[8],
								m->values[0] * m->values[8] - m->values[2] * m->values[6],
								m->values[2] * m->values[3] - m->values[0] * m->values[5],
								m->values[3] * m->values[7] - m->values[4] * m->values[6],
								m->values[1] * m->values[6] - m->values[0] * m->values[7],
								m->values[0] * m->values[4] - m->values[1] * m->values[3] };

	Matrix * result = create_Matrix(r);
	result->mod(result, n);
	return result;
}

/**
 *	calculate Mod n for entire matrix cell by cell
 */
void module(Matrix * m, unsigned long long n) {

	int i, v;
	for(i = 0; i < 9; i++) {
		m->values[i] = ModS12(m->values[i], n);
	}
}


/**
 *	construct a new matrix and allocates space
 */
Matrix * create_Matrix(unsigned long long * values) {

	Matrix * m = malloc(sizeof(Matrix));
	m->values = malloc(9 * sizeof(unsigned long long));
   	memcpy(m->values, values, 9 * sizeof(unsigned long long));
	
	// set pointer to property functions...
	m->repr = print_Matrix; 
	m->det = get_Det;
	m->mult_v = mul_matrix_v;
	m->mult_m = mul_matrix_m;
	m->mod = module;
	m->trans = transpose;
	m->adj = adjungate;
	
	return m;
}


/**
 *	release the memorey of matrix
 */
void destroy_Matrix(Matrix * m) {

	assert(m != NULL);
	free(m->values);
	free(m);
}


