#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"


unsigned long long power(unsigned long long n, unsigned long long e) {
    
    unsigned long long r = 1;
    
    int i;
    for(i = 0; i < e; i++)
        r *= n;
    
    return r;
}

int binToDec(int * ar, int n) {

    int i, sum = 0;
    for(i = 0 ;i < n; i++) {
        //printf("value: %d\n", ar[i]);
        
        if(ar[i] == 1) {

            int r = power(2, n-i-1);

          //  printf("%d, %d\n", i, r);
            sum += r;
                
        }
        
    }
        
    return sum;
}



unsigned long long log2n( unsigned long long x )
{
	unsigned long long ans = 0 ;
	while( x>>=1 ) ans++;
	return ans ;
}

char * tobinstr(unsigned long long value) {
    
    unsigned long long bitsCount = log2n(value) + 1;

  	char * output = malloc(sizeof(char) * bitsCount);  
    output[bitsCount] = '\n';
    
    int i;
  	for (i = bitsCount - 1; i >= 0; --i, value >>= 1)
        output[i] = (value & 1) + '0';
    
    return output;
}

char * readFromFile(char * fileName) {

  
  char *content;
  long inputFileSize;
  
  FILE * inputFile;

  if (inputFile = fopen(fileName, "rb")) {
    fseek(inputFile, 0, SEEK_END);
    inputFileSize = ftell(inputFile);
    rewind(inputFile);
    content = malloc(inputFileSize * (sizeof(char)));
    fread(content, sizeof(char), inputFileSize, inputFile);
    fclose(inputFile);
  } else {
    printf("can't open file '%s'\n", fileName);
    exit(0);
  }

  return content;
}


char toLower(char value){
  return value >= 'A' && value <= 'Z' ? value + 32 : value;
}

char toUpper(char value){
  return value >= 'a' && value <= 'z' ? value - 32 : value;
}