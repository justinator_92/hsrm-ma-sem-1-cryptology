#ifndef _CRYPTLIB_H
#define _CRYPTLIB_H

/**
 *	Berechnet die Lösung der Gleichung ggT(a, b) = a*x + b*y
 */
unsigned long long eEAS12(unsigned long long a, unsigned long long b, long long* x, long long* y);


/*
 *	Berechnet Lösung der Diophantischen Gleichung
 */
unsigned long long eEAdS12(unsigned long long a, unsigned long long b, unsigned long long d, long long* x, long long* y);


/*
 *	liefert den Wert der n-ten Primzahl
 */
unsigned long long prime1S12(unsigned long long n);

/**
 *	liefert den Wert einer zufällig im Intervall [a, b] gewählten Primzahl
 */
unsigned long long primeZS12 (unsigned long long a, unsigned long long b);

/**
 *	berechnet die Primzahlen p mit 2<= p <= n. Ergebnisübergabe per call-by-reference
 */
unsigned long long primeNS12(unsigned long long* ptr, unsigned long long n);


/**
 *	liefert den Wert 0 falls n eine Primzahl ist, oder 1 falls n keine Primzahl ist
 */
unsigned int isPrimeS12(unsigned long long n);


/**
 *	Ermittelt die Pseudozufallsfolge gemäß für genau einen
 *	Iterationsschritt in Abhängigkeit des Startwertes x
 */
unsigned long long RandS12(unsigned long long a, unsigned long long b, unsigned long long n, unsigned long long x);


/**
 *	Berechnet Wert des ggT unter Verwendendung des 
 *	euklidschen Algorithmus
 */
unsigned long long ggTS12(unsigned long long a, unsigned long long b);


/**
 *	Berechnet die eulersche Phi Funktion
 */
unsigned long long EulPhiS12(unsigned long long n);


/**
 *	liefert den ganzzahligen Anteil der Division von a und n
 */
unsigned long long DivS12(long long a, long long n);


/**
 *	liefert den Rest der Division von a und n
 */
unsigned long long ModS12(unsigned long long a, unsigned long long n);


/**
 *	liefert die Inverse zu a module n.
 * 	Falls keine Inverse existiert ist Rückgabewert NULL
 */
unsigned long long ModInvS12(unsigned long long a, unsigned long long n); 

/**
 *	berechnet die b-te Potenz von a module n. 
 *	d.h. a^b mod n gemäß Square-and-multiply algorithmus
 *	und dem geib'schen Entwicklungssatz
 */
unsigned long long ModExpS12(unsigned long long a, unsigned long long b, unsigned long long n);



/**
 *	Berechnet den Wert der affinen Tauschchiffre
 *	d.h. die funktion berechnet: (p * t + k) Mod n
 *	
 */
unsigned long long affine(int p, unsigned long long t, unsigned long long k, unsigned long long n);


/**
 *	Berechnet den inversen Wert der Tauschchiffre
 *	Benutzt zur berechnung das modulare inverse von t und n
 *	Macht folgende Berechnung: (c - k * Inv(t, n)) Mod n
 */
unsigned long long affineInv(int c, unsigned long long t, unsigned long long k, unsigned long long n, unsigned long long inv);

#endif