#ifndef _UTILS_H
#define _UTILS_H

/**
 *  Library for util methods
 *
 */

unsigned long long log2n( unsigned long long x );

char * tobinstr(unsigned long long value);

char * readFromFile(char * fileName);

char toLower(char value);

char toUpper(char value);

unsigned long long power(unsigned long long n, unsigned long long e);

int binToDec(int * ar, int n);

#endif