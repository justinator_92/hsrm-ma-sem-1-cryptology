#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

/**
 *	This is a matrix class that provides basic
 * 	factionality like calculating determinants
 * 	inverting
 * 	
 * 	@author: Justin Albert
 *
 */

struct _matrix {

	int cols, rows;
	int * values;

	void (*repr)(struct _matrix *);
	int (*det)(struct _matrix *);
	struct _matrix * (*mult_v)(struct _matrix *, int value);
	struct _matrix * (*mult_m)(struct _matrix *, struct _matrix *);
	void (*mod)(struct _matrix *, int n);
	struct _matrix * (*trans)(struct _matrix *);
	struct _matrix * (*inv)(struct _matrix *, int n);
	struct _matrix * (*adj)(struct _matrix *);

} typedef Matrix;


// forward declarations...
Matrix * mul_matrix_v(Matrix * m, int value);
Matrix * mul_matrix_m(Matrix * m1, Matrix * m2);
Matrix *create_Matrix(int row, int col, int values[]);

/*
 *	helper method to get linear index for two dimensions...
 */
int helper_Idx(int x, int y, int cols) {
	return y * cols + x;
}


/*
 *	console representation for matrix m
 *	printed out
 */
void print_Matrix(Matrix * m) {

	int x, y;	
	printf("M = \n");
	for(y = 0; y < m->rows; y++) {
		printf("%c ", '|');
		for(x = 0; x < m->cols; x++) {
			printf("%d ", m->values[helper_Idx(x, y, m->cols)]);
		}
		printf("%c\n", '|');
	}
	
}


/**
 *	calculate determinant of Matrix m
 * 	uses rule of sarrus, so it's only working for A1,1 A2,2 and A3,3
 */
int get_Det(Matrix * m) {
	
	int row = m->rows;
	int col = m->cols;

	assert(row == col);
	
	if(row == 1) return m->values[0];
	if(row == 2) return m->values[0] * m->values[3] - m->values[2] * m->values[1];

	int x, y, det = 0;

	for(y = 0; y < row; y++) {
		int p = 1;	
		for(x = 0; x < col; x++) {
			int a = m->values[helper_Idx((y+x)%col, x, col)];
			//printf("d = %d\n", a);
			p *= a;
		}
		//printf("%d\n", p);
		det += p;
	}

	for(y = 0; y < row; y++) {
		int p = 1;	
		for(x = 0; x < col; x++) {
			int a = m->values[helper_Idx((y+x)%col, row - x - 1, col)];
			//printf("d = %d\n", a);
			p *= a;
		}
		//printf("%d\n", p);
		det -= p;
	}

	return det;
}

Matrix *mul_matrix_v(Matrix * m, int value) {

	int i;
	for(i = 0; i < m->rows * m->cols; i++)
		m->values[i] = m->values[i] * value;
	return m;
}

Matrix *mul_matrix_m(Matrix * m1, Matrix * m2) {

	int m1Rows = m1->rows;
	int m1Cols = m1->cols;
	int m2Rows = m2->rows;
	int m2Cols = m2->cols;

	assert(m1Cols == m2Rows);

	int r[m1Rows * m2Cols];

	int c, d, k, sum = 0;

	for (c = 0; c < m1Rows; c++) {
		for (d = 0; d < m2Cols; d++) {
        		for (k = 0; k < m2Rows; k++) {
          			sum = sum + m1->values[helper_Idx(k, c, m1Cols)] * m2->values[helper_Idx(d, k, m2Cols)];
				}
				r[helper_Idx(d, c, m2->cols)] = sum;
        		sum = 0;
      		}
    	}
	

	Matrix * result = create_Matrix(m1Rows, m2Cols, r);
	return result;
}

Matrix * invert(Matrix * m, int n) {

	assert(m->cols == m->rows);

	Matrix * adj = m->adj(m);
	//adj->repr(adj);

	int detA = adj->det(adj); 
	
	if(detA < 0) {
		detA *= -1;
		adj->mult_v(adj, -1);
		//adj->repr(adj);
	}

	int inv = ModInv(detA, n);
	assert(inv != 0); // to make sure a inverse exists
	

	adj->mult_v(adj, inv);
	adj->mod(adj, n);

	return adj;
}


Matrix *transpose(Matrix * m) {

	assert(m->cols == m->rows);

	int r[m->rows * m->cols];
	Matrix * result = create_Matrix(m->rows, m->cols, r);

	int x, y;

	for (y = 0; y < m->rows; y++) 
		for (x = 0; x < m->cols; x++) 
        		result->values[helper_Idx(y, x, m->cols)] = m->values[helper_Idx(x, y, m->cols)];

	return result;
}

Matrix *adjungate(Matrix * m) {

	assert(m->cols == m->rows);

	int r[m->rows * m->cols];
	
	if(m->cols == 1)
		return m;

	Matrix * result = create_Matrix(m->rows, m->cols, r);

	if(m->cols == 2) {
		result->values[0] = m->values[3];
		result->values[1] = m->values[1] * -1;
		result->values[2] = m->values[2] * -1;
		result->values[3] = m->values[0];
	}


	return result;
}


void module(Matrix * m, int n) {

	int i, v;
	for(i = 0; i < m->rows * m->cols; i++){
		v = m->values[i];
		if(v >= 0) {
			v = v % n;
		} else {
			v = (v + (100 * n)) % n;
		}
		m->values[i] = v;
	}
	
}

Matrix *create_Matrix(int col, int row, int * values) {

	Matrix * m = malloc(sizeof(Matrix));
	m->rows = row;
	m->cols = col;
	
	int len = col * row;

	m->values = malloc(len * sizeof(int));
   	memcpy(m->values, values, len * sizeof(int));
	
	// set pointer to property functions...
	m->repr = print_Matrix; 
	m->det = get_Det;
	m->mult_v = mul_matrix_v;
	m->mult_m = mul_matrix_m;
	m->mod = module;
	m->trans = transpose;
	m->inv = invert;
	m->adj = adjungate;
	
	return m;
}



void destroy_Matrix(Matrix * m) {

	assert(m != NULL);
	//free(m->values);
	free(m);
}


