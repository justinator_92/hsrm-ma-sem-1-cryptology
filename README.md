# README #

This repository contains all exercises for the lecture "cryptology" in winter term 15/16 for my master studies at the University of Applied Sciences in Wiesbaden known as HSRM. The sourcecode is mostly written in pure C, but small parts are also written in Python. 

### How do I get set up? ###

* Download the repository
* make cd into the source code folder of your choice
* execute the according Makefile
* run the application with ./application