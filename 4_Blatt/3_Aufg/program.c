#include "lfsr.h"
#include "../../crypto.h"
#include <stdlib.h>

int power(int n, int e) {
    int i, r = 1;
    for(i = 0; i < e; i++)
        r *= n;
    
    return r;
}

int binToDec(int * ar, int n) {

    int i, sum = 0;
    for(i = 0 ;i < n; i++) {
        
        if(ar[i] == 1) {

            int r = power(2, n-i-1);
            sum += r;
                
        }
        
    }
        
    return sum;
}

char * crypt(char * text, LFSR * gen) {

    char * ret = malloc(strlen(text) * sizeof(char));
    char * handler = text;
    char * rHandler = ret;
   
    while(*handler) {
        
        int a = binToDec(gen->vector, gen->n) ^ (*handler++ - 'a');
        *rHandler++ = a + 'a';

        gen->gen(gen);
    }

    return ret;
}


int main(void) {

    

    int startVector[] = { 1, 0, 1, 0, 1, 0, 1, 0 };    
    char * content = "Jede Sicherheitslücke ist zunächst auf ihre Ausnutzbarkeit hin zu untersuchen";

    // encrypt
    LFSR * shift = createLFSR(startVector, 8, 4);
    char * cipher = crypt(content, shift);
    printf("ciphertext: %s\n", cipher);

    // decrypt
    shift = createLFSR(startVector, 8, 4);
    char * plain = crypt(cipher, shift);
    printf("plaintext: %s\n", plain);
    
    return 0;
} 