#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "lfsr.h"


void _tick(LFSR * sr) {

    int i, n = sr->n;

    int temp[n];
    memcpy(temp, sr->vector, n * sizeof(int));

    for(i = 0; i < n; i++) {

        if(i == 0) {
            sr->vector[i] = temp[n - 1] ^ temp[n - sr->m];
        } else {
            sr->vector[i] = temp[((i - 1) + n) % n];    
        }
    }

}

void _gen(LFSR * sr) {

    int i;
    for(i = 0; i < sr->n; i++) {
        sr->tick(sr);
    }

}


LFSR * createLFSR(int * vector, int n, int m) {

    LFSR * ret = malloc(sizeof(LFSR));

    if(ret == NULL)
        return NULL;

    ret->vector = malloc(n * sizeof(int));
    memcpy(ret->vector, vector, n * sizeof(int));
    ret->n = n;
    ret->m = m;

    ret->gen = _gen;
    ret->tick = _tick;

    return ret;
}

void destroyLFSR(LFSR * gen) {

    free(gen->vector);
    free(gen);

}

void print(LFSR * reg) {

    int i;
    for(i = 0; i < reg->n; i++) {
        printf("%d,", reg->vector[i]);
    }
    printf("\n");
}