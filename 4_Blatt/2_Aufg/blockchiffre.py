#!/usr/bin/python

import collections
import copy

class Register():

	def __init__(self, keys = []):
		self.init = copy.copy(keys)
		self.keys = keys

	def gen(self, n):
		
		while(True):
			yield self.keys
			self.keys = self.keys[n:] + self.keys[:n]

	def reset(self):
		self.keys = copy.copy(self.init)
			
	def __repr__(self):
		return "register" + str(self.keys)

class Block():

	def __init__(self, registers = []):
		self.regs = registers

	def crypt(self, m, jump = 1):

		registers = [x.gen(jump) for x in self.regs]
		ret = []

		for c in m:

			a = 0

			for r in registers:
				a = a ^ r.next()[0]

			ret.append(a ^ c)

		return ret

	def reset(self):
		for r in self.regs:
			r.reset()


if __name__ == '__main__':

	key = [0, 1, 1, 0, 1, 0, 1, 1]

	reg1 = Register(key[:5])
	reg2 = Register(key[5:])

	text = [1, 0, 0, 1, 0, 1, 1, 0]

	block = Block([reg1, reg2])
	cipher = block.crypt(text)
	print cipher

	block.reset()
	plain = block.crypt(cipher)

	print plain

	