#!/usr/bin/python

import copy

class BlockChiffre():

	def __init__(self, keys = [], inputs = [], permutations = {}):
		self.keys = keys
		self.inputs = inputs
		self.p = permutations


	def xor(self, key, m1, m2):
		r = []
		
		for k, m in zip(key, m1+m2):
			r.append(k ^ m)

		return r

	def crypt(self):
		
		count = 0

		while(True):

			r = []

			it = iter(self.inputs)
			for x in it:
				res = self.xor(self.keys[count % len(self.keys)], x, next(it))
				r.append(res[0 : len(res) / 2])
				r.append(res[len(res) / 2 : len(res)])

			yield r

			n = copy.copy(r)

			for k, v in self.p.iteritems():
				r[k] = n[v]

			self.inputs = copy.copy(r)
			count += 1





if __name__ == '__main__':

	key1 = [1, 0, 1, 0, 1, 0, 1, 0]
	key2 = [1, 1, 0, 0, 1, 1, 0, 0]
	key3 = [0, 0, 1, 1, 0, 0, 1, 1]
	
	m1 = [1, 0, 0, 1]
	m2 = [0, 1, 1, 0]
	m3 = [1, 1, 0, 0]
	m4 = [0, 0, 1, 1]

	block = BlockChiffre([key1, key2, key3], [m1, m2, m3, m4], {1 : 2, 2 : 1})
	gen = block.crypt()
	gen.next()
	gen.next()
	res = gen.next()

	print res

	block2 = BlockChiffre([key1, key2, key3], res, { 1: 2, 2: 1})
	gen1 = block2.crypt()
	gen1.next()
	gen1.next()
	print gen1.next()
