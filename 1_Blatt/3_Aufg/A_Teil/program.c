#include <stdio.h>
#include <stdlib.h>
#include "../../../crypto.h"


/**
 *	program is decrypting text from given textfile
 *	@author: Justin Albert
 */
int main(int argc, char * argv[]){

	if(argc < 3) {
		printf("must call ./program> 'fileA.txt' 'fileB.txt'\n");
		exit(0);
	}

	char *input = argv[1];
	char *output = argv[2];
	
	remove(output);
	
	char * content = readFromFile(input);
	char *handler;

	FILE * out;
	out = fopen(output, "wb");
	
	int n;
	for(n = 0; n <= 26; n++) {  
		fprintf(out, "\ntesting new key: n = %d\n", n);  // testing all keys n = 0,..,26
		handler = content;	
		while(*handler != '\n')
			fprintf(out, "%c", rotChar(toLower(*handler++), n));
	}
	
	fclose(out);
	return 0;
}

