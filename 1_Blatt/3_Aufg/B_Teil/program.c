#include <stdio.h>
#include <stdlib.h>
#include "../../../crypto.h"


static const char lookUpTable [26] = {
'U', 'V', 'X', 'Z', 'N', 'E', 'W', 'Y', 'O', 'R', 'K', 'C', 'I', 'T', 'A', 'B', 'D', 'F', 'G', 'H', 'J', 'L', 'M', 'P', 'Q', 'S'
};

char getLookup(char c) {
	
	int i;
	for(i = 0; i < 26; i++) {
		if(lookUpTable[i] == c)
			return i + 'a';
	}
	return c;
}


int main(int argc, char * argv[]){

	if(argc < 3) {
		printf("must call ./<program> <fileA.txt> <fileB.txt>\n");
		return -1;
	}

	char *content = readFromFile(argv[1]);
	char *handler = content;

	FILE *outputFile = fopen(argv[2], "w");
	
	while(*handler != '\n') {
		
		if(*handler == ' ') {
			*handler++; continue;
		}

		fprintf(outputFile, "%c", getLookup(*handler++));
	}
	
	fclose(outputFile);

	return 0;
}

