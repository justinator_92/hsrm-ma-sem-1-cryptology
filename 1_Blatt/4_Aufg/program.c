#include <stdio.h>
#include <stdlib.h>

void verschluessler(char * plainText, char * cypherText) {

	char * handler = plainText;
	char * cypher = cypherText;
	
	while(*handler != '\n') {
		if(*cypher == '\n') cypher = cypherText;
		*handler++ = ((*handler - 32) ^ (*cypher++ - 32)) + 32;		
	}
}


int main( void ) {

	char text[] = "Dies ist der gegebene Klartext\n" ;
	char geheimnis[] = "GEHEIM\n";

	printf("Vor der Verschlüsselung: %s\n", text) ;

	verschluessler(text, geheimnis) ;
	printf("Nach der Verschlüsselung: %s\n", text) ;

	verschluessler(text, geheimnis) ;
	printf("Nach der Entschlüsselung: %s\n", text) ;
	return 0 ;
}
