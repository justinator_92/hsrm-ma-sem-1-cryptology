#include <stdio.h>
#include "../../crypto.h"


int main(int argc, char * argv[]){

	if(argc < 3) {
		printf("must call ./<program> <fileA.txt> <fileB.txt>\n");
		return -1;
	}

	char * input = readFromFile(argv[1]);
	FILE * out = fopen(argv[2], "w");
	
	while (*input != '\n') fprintf(out, "%c", *input++);
	fclose(out);	

	return 0;
}

