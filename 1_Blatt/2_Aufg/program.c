#include <stdio.h>
#include <crypto.h>

int main(int argc, char * argv[]){

	if(argc < 3) {
		printf("must call ./<program> <int a> <int b>\n");
		return -1;
	}

	int a = atoi(argv[1]);
	int b = atoi(argv[2]);

	printf("ggt(%d, %d) = %d\n", a, b, ggT(a, b));
	printf("kgV(%d, %d) = %d\n", a, b, kgV(a, b));
	

	return 0;
}

