#include <stdio.h>
#include "../../../crypto.h"


/**
 *	application calculates eulersche phi function
 * 	for given integer n
 */
int main(int argc, char * argv[] ) {

	if(argc < 2) {
		printf("must call ./program> 'int'\n");
		return -1;
	}

	int n = atoi(argv[1]);

	printf("Der Zn Restklassenring von %d lautet:\n", n);
	MultGroup(n);
	
	return 0;
}

