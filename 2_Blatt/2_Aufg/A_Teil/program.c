#include <stdio.h>
#include "../../../crypto.h"


/**
 *	application calculates eulersche phi function
 * 	for given integer n
 */
int main(int argc, char * argv[] ) {

	if(argc < 2) {
		printf("must call ./program> 'int'\n");
		return -1;
	}

	int n = atoi(argv[1]);

	printf("Die eulersche Phi-Funktion von %d lautet: %d. \n", n, EulerPhi(n));
	
	return 0;
}

