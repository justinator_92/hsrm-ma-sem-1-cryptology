#include <stdio.h>
#include <time.h>
#include "../../../crypto.h"


struct _primeGen {

	int n; // last calculated prim number
	int (*next)(struct _primeGen *);

} typedef PrimeGen;


int genNext(PrimeGen * gen) {

	int cur = gen->n;
	int i, flag = 1;

	while(1) {

		flag = 1;

		cur++;

		for(i = 2; i < cur; i++) {

			if(cur % i == 0){
				flag = -1;
				break;
			}

		}

		if(flag == 1) {
			gen->n = cur;
			return cur;
		}

	}

}

PrimeGen * getPrimeGen() {

	PrimeGen * gen = malloc(sizeof(PrimeGen));
	gen->n = 2;

	gen->next = genNext;

	return gen;
}

void primGenerator(int n) {

	int i, j, flag = 1;

	for(i = 2; i < n; i++) {

		flag = 1;

		for(j = 2; j < i; j++) {

			if(i%j == 0) {
				flag = 0;
				break;
			}
				
		}

		if(flag){
			printf("DIe n-te primzahl lautet: %d\n", i);
		}
	}

}

/**
 *	Method creates pseudo-random numbers limited by module m
 * 	@param: a
 * 	@param: b
 *	@param: m
 *	@param: x0
 *
 */
void BBSGenerator() {

	PrimeGen * gen = getPrimeGen();

	srand(time(NULL));
	gen->n = rand() % 100;  // generate random start value betwenn 0 .. 1000

	int i, p, q, n;

	p = gen->next(gen);
	while(p % 4 != 3)
		p = gen->next(gen);

	q = gen->next(gen);
	while(q % 4 != 3)
		q = gen->next(gen);

	n = p * q;

	printf("p: %d, q: %d, n=pq: %d\n", p, q, n);
	

	int s = gen->next(gen);

	int x = (s * s) % n;

	for(i = 1; i < 10; i++) {
		x = (x * x) % n;
		printf("x%d: %d, %d\n",i, x, x % 2);
	}

	free(gen);
}



int main(int argc, char * argv[] ) {

	PrimeGen * gen = getPrimeGen();

/*	printf("Der Generator liefert: %d\n", gen->next(gen));
	printf("Der Generator liefert: %d\n", gen->next(gen));
	printf("Der Generator liefert: %d\n", gen->next(gen));
	printf("Der Generator liefert: %d\n", gen->next(gen));
	printf("Der Generator liefert: %d\n", gen->next(gen));
	printf("Der Generator liefert: %d\n", gen->next(gen));
	printf("Der Generator liefert: %d\n", gen->next(gen));*/

	BBSGenerator();

	return 0;
}

