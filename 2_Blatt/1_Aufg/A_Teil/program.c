#include <stdio.h>
#include "../../../crypto.h"

/**
 *	application calculates module inverse number
 * 	for given a and modul n
 */
int main(int argc, char * argv[] ) {

	if(argc < 3) {
		printf("must call ./<program> <int a> <int b>\n");
		return -1;
	}

	int a = atoi(argv[1]);
	int n = atoi(argv[2]);

	printf("Die modulare Inverse von %d und %d ist: %d\n", a, n, ModInv(a, n));
	
	return 0;
}

