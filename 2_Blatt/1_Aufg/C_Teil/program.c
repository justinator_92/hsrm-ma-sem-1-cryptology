#include <stdio.h>
#include "../../../crypto.h"


void decryptBlocks(int col, int row, char * text, Matrix * k, int n) {

	int len = strlen(text) / row;
	printf("invert key matrix:\n");
	Matrix * kInv = k->inv(k, n);  // invert key matrix
	kInv->repr(kInv);

	int temp[row * col];  // buffer array for new created cipher matrices...
	int i;

	while(*text != '\n') {

		for(i = 0; i < row; i++) 
			temp[i] = *text++ - 'A';
		
		// build matrix
		Matrix * ciph = create_Matrix(col, row, temp);
		Matrix * res = kInv->mult_m(kInv, ciph);
		res->mod(res, n);

		for(i = 0; i < row; i++) 
			printf("%c", res->values[i] + 'A');
	}
}


/**
 *	application calculates module inverse number
 * 	for given a and modul n
 */
int main(int argc, char * argv[] ) {

	char * content = "UBIXGTENERHLNHAHRM\n";
	
	int ka[] = { 0, 1, 7, 15 };
	Matrix * k = create_Matrix(2, 2, ka);  // create key matrix...
	
	decryptBlocks(1, 2, content, k, 27);
	return 0;
}

