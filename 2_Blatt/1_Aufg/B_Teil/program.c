#include <stdio.h>
#include "../../../crypto.h"

/**
 *	application calculates module inverse number
 * 	for given a and modul n
 */
int main(int argc, char * argv[] ) {

	int a[] = { 5 };
	Matrix * p = create_Matrix(1, 1, a);
	p->repr(p);
	
	int b[] = { 17 };
	Matrix * k = create_Matrix(1, 1, b);
	k->repr(k);

	Matrix * c = hillEncrypt(p, k, 26);
	c->repr(c);

	Matrix * p1 = hillDecrypt(c, k, 26);
	printf("plainText:\n");
	p1->repr(p1);
	
	return 0;
}