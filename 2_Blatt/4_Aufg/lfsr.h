struct _lfsr {

    int n;  // length of shift vector
    int m; // periodic xor element;
    int * vector;
    void(*gen)(struct _lfsr *);
    void(*tick)(struct _lfsr *);
} typedef LFSR;


void _tick(LFSR * sr);
void _gen(LFSR * sr);
LFSR * createLFSR(int * vector, int n, int m);
void destroyLFSR(LFSR * gen);
void print(LFSR * reg);