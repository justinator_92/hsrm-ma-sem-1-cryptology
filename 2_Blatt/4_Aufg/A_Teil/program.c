#include "../lfsr.h"
#include <stdio.h>

int main(void) {

    int startVector[] = { 1, 0, 1, 0, 1, 0, 1, 0 };

    LFSR * shift = createLFSR(startVector, 8, 4);
    printf("Generator created...%d\n", shift->n);
    print(shift);

    int i;
    for(i = 0; i < 256; i++) {
        shift->gen(shift);
        print(shift);
    }

    return 0;
} 