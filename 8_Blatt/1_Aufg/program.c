#include <stdio.h>
#include <math.h>
#include "../../cryptlib/cryptlib.h"


unsigned long long squareRoot(unsigned long long input) {
    
    unsigned long long op  = input;
    unsigned long long res = 0;
    unsigned long long one = 1uL << 30; // The second-to-top bit is set: use 1u << 14 for uint16_t type; use 1uL<<30 for uint32_t type

    // "one" starts at the highest power of four <= than the argument.
    while (one > op)
        one >>= 2;

    while (one != 0) {
        if (op >= res + one) {
            op = op - (res + one);
            res = res +  2 * one;
        }
        res >>= 1;
        one >>= 2;
    }
 
    return res;
}



void primeFactors(unsigned long long a) {
	if(a <= 1)
		return;

	if(isPrimeS12(a) == 0)
		return;

	unsigned long long rest = a;
	unsigned long long d = 2;

	unsigned long long root = squareRoot(a);

	int counter = 0;

	while(d <= root) {
		if(rest % d == 0) {
			rest /= d; counter += 1;
		} else {
			if (counter > 0) printf("%lld ^ %d\n", d, counter);
			d = d == 2 ? 3 : d+2; counter = 0;
		}
	}
}

int main(int argc, char * argv[] ) {

	if(argc >= 2) {
		unsigned long long t = atoi(argv[1]); 
		printf("Die Primfaktoren von %lld lauten: \n", t);
		primeFactors(t);
	} else {
		printf("Bitte geben Sie eine Zahl > 2 an:\n");
	}

	return 0;
}

