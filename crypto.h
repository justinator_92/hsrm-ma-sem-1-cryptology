#include <stdlib.h>
#include "matrix.h"

/**
 * 	This is the C header file
 * 	for all functions regarding the lecture cryptology
 * 	hold in the winter term 2015/16
 * 	at the University of applied sciences in Wiesbaden
 * 	@author: Justin Albert
 * 	@Date: Nov 11th 2015
 */ 


/**
 * 	File and string operations...
 *
 */

char * readFromFile(char * fileName) {

	
	char *content;
	long inputFileSize;
	
	FILE * inputFile;

	if (inputFile = fopen(fileName, "rb")) {
		fseek(inputFile, 0, SEEK_END);
		inputFileSize = ftell(inputFile);
		rewind(inputFile);
		content = malloc(inputFileSize * (sizeof(char)));
		fread(content, sizeof(char), inputFileSize, inputFile);
		fclose(inputFile);
	} else {
		printf("can't open file '%s'\n", fileName);
		exit(0);
	}

	return content;
}


char toLower(char value){
	return value >= 'A' && value <= 'Z' ? value + 32 : value;
}

char toUpper(char value){
	return value >= 'a' && value <= 'z' ? value - 32 : value;
}

/**
 * 	cryptologic operations
 *
 */


char rotChar(char c, int n) {
	c = toLower(c);
	if(c < 97 || c > 123)  // if char is not a lower case letter...
		return c;
	return ((c - 'a' + n) % 26) + 'a';
}



/**
 * 	calculates ggT
 */
int ggT(int a, int b) {

	int c = a % b;
	
	while(c != 0) {
		a = b;
		b = c;
		c = a % b;
	}
	return b;
}

/**
 * 	calculates the smallest common divider
 */
int kgV(int a, int b) {
	return (a * b) / ggT(a, b);
}


struct _tuple {
	int d;
	int x;
	int y;
} typedef Tuple;


Tuple ErwEuklid(int a, int b) {
	
	if(b == 0) {
		Tuple ret = { a, 1, 0 };	
		return ret;
	}
	Tuple t = ErwEuklid(b, a % b);
	Tuple r = { t.d, t.y, t.x - ((a - (a % b)) / b) * t.y }; 	
	return r; 
}



int ModInv(int a, int n) {

	if(ggT(a, n) != 1)
		return 0;

	Tuple t = ErwEuklid(a, n); 
	return t.x;
}


/**
 *	Method to encrypt message as given nxn matrix with module n
 *
 *	@param p: plain text matrix
 * 	@param k: key matrix
 *	@param n: modul
 *
 * 	@returns: cyphertext matrix
 */
Matrix * hillEncrypt(Matrix * p, Matrix * k, int n) {

	assert(p->rows == p->cols == k->rows == k->cols);
	int det = k->det(k);
	assert(ggT(det, n) == 1);
	assert(ModInv(det, n) != 0); // to make sure a inverse exists
	
	Matrix * c = k->mult_m(k, p); 
	c->mod(c, n);
	return c;
}


/**
 *	Method to decrypt message as given nxn matrix with module n
 *
 *	@param p: plain text matrix
 * 	@param k: key matrix
 *	@param n: modul
 *
 * 	@returns: cyphertext matrix
 */
Matrix * hillDecrypt(Matrix * c, Matrix * k, int n) {
	
	assert(k->cols == c->rows);
	k->inv(k, n);
	Matrix * p = k->mult_m(k, c);
	p->mod(p, n);
	return p;
}


int EulerPhi(int n) {

	int i, counter = 0;
	for(i = 0; i < n; i++) {
		if(ggT(i, n) == 1)
			counter += 1;
	}

	return counter;
}


void MultGroup(int n) {

	int i;
	for(i = 0; i < n; i++) {
		if(ggT(i, n) == 1)
			printf("%d\n", i);
	}

}