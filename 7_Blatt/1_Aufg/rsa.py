#!usr/bin/env python

import random, math

alphabet = "abcdefghijklmnopqrstuvwxyz"

def primeGen(min, max):
    """ generates a pseudo prime number using fermats satz """
    rand = random.randint(min, max)
    while fermat(rand) != 1 or not isPrime(rand):
        rand += 1
    return rand


def fermat(n):
    """ fast prime test using fermats satz """
    return 2**(n-1) % n


def eulerTot(n):
	""" calculates euler totient function """
	if n == 1: return n
	if isPrime(n): return n - 1
	res = n
	if n % 2 == 0:
		res -= res / 2
		while n % 2 == 0: n /= 2

	for j in range(3, int(math.sqrt(n)) + 1, 2):
		if n % j == 0:
			res -= res / j
			while n % j == 0: n /= j

	if n > 1: res -= res / n
	return res
	
def genPubKey(limit):
	""" generates a public key """
	rand = random.randint(int(math.sqrt(limit)), limit)
	while gcd(rand, limit)  != 1:
		rand += 1
	return rand


def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

def isPrime(n):
	""" check if given number is prime """
	for i in range(2, int(math.sqrt(n))):
		if n%i == 0:
			return False
	return True

def gcd(a, b):
	if b == 0: return a
	return gcd(b, a%b)


def generateKeys(min, max):
	p, q = primeGen(2**min, 2**max), primeGen(2**min, 2**max)
	n = p * q
	phi = (p - 1) * (q - 1)
	Pk = genPubKey(n)
	print p, q, n, Pk
	Sk = modinv(Pk, phi)
	return (Pk, n), (Sk, n)

def encrypt(m, pubk):
	return m ** pubk[0] % pubk[1]

def decrypt(c, prik):
	return c ** prik[0] % prik[1]


def makeBlocks(m, s = 128, b = 2):
	
	for _ in range(0, len(m) % b): m += 'a' # make size dividable by block size...

	ret = []
	for a, b in zip(m[0::2], m[1::2]):
		ret.append(ord(a) * s**1 + ord(b) * s**0)

	return ret



def makeString(lis, s = 128):
	ret = ""
	for l in lis:
		ret += chr(l / s)
		ret += chr(l % s)
	return ret;

def encryptText(text, pubk):
	enc = []
	lis = makeBlocks(text)
	for l in lis:
		enc.append(encrypt(l, pubk))
	return enc

def decryptText(lis, prik):
	ret = []
	for l in lis:
		ret.append(decrypt(l, prik))
	return makeString(ret)



if __name__ == '__main__':
	keys = generateKeys(8, 9)
	print keys
	cipher = encryptText("Public-Key", keys[0])
	print cipher
	plain = decryptText(cipher, keys[1])
	print plain	