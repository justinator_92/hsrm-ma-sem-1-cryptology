#!usr/bin/env python

import random

def genKey(p, g):
	a = random.randint(0, p-2)
	alpha = g**a % p
	return (alpha, a)

def getKey(a, b, p):
	return a**b % p


if __name__ == '__main__':
    p, g = 13, 2
    A, B = genKey(p, g), genKey(p, g)
    print A, B
    print getKey(B[0], A[1], p)
    print getKey(A[0], B[1], p)
