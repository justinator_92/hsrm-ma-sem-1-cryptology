#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../../cryptlib/cryptlib.h"
#include "../../cryptlib/matrix.h"


void test() {

	int ar[] = { 1, 0, 0,
				 0, 1, 0,
				 0, 0, 1 };

	Matrix * a = create_Matrix(3, 3, ar);
	a->repr(a);

	Matrix * inv = a->inv2(a, 26);

	inv->repr(inv);

	destroy_Matrix(inv);
	destroy_Matrix(a);
}


/**
 *	Test application for cryptlib library
 *
 */
int main(void) {

	int ar[] = { 6, 24, 1,
				 13, 16, 10,
				 20, 17, 15 };

	Matrix * m1 = create_Matrix(3, 3, ar);
	m1->repr(m1);

	Matrix * inv = m1->inv2(m1, 26);
	inv->repr(inv);

	Matrix * e = inv->mult_m(inv, m1);
	e->mod(e, 26);

	e->repr(e);

	destroy_Matrix(m1);

//	test();

	return 0;
}

