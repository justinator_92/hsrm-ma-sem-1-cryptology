#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../../cryptlib/cryptlib.h"



/**
 *	Test application for cryptlib library
 *
 */
int main(int argc, char * argv[] ) {

	unsigned long long x = 7323;
	unsigned long long y = 2469;
	unsigned long long a = 97;
	unsigned long long b = 1589316386;

	unsigned long long z = x * y;

	printf("z = %llu * %llu = %llu\n", x, y, z);

	unsigned long long phi = EulPhiS12(z);

	printf("phi(%llu) = %llu\n", z, phi);

	unsigned long long r = ModS12(b, phi);

	printf("r = %llu mod %llu\n = %llu\n", b, phi, r);

	unsigned long long S1 = ModExpS12(a, r, z);
	unsigned long long S2 = ModExpS12(a, b, z);

	printf("S1: %llu ^ %llu mod %llu = %llu\n", a, r, z, S1);
	printf("S2: %llu ^ %llu mod %llu = %llu\n", a, b, z, S2);

	return 0;
}

