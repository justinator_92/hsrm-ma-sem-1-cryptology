#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../../cryptlib/cryptlib.h"


void testEEA() {

	long long x1 = 1;
	long long y1 = 1;

	long long *x = &x1;
	long long *y = &y1;

	unsigned long long p = 62;
	unsigned long long q = 9;
	printf("%llu\n", eEAS12(p, q, x, y));

	printf("x: %lld\n", *x);
	printf("y: %lld\n", *y);
}

void testEEAd() {

	long long x1 = 1;
	long long y1 = 1;

	long long *x = &x1;
	long long *y = &y1;

	unsigned long long p = 23;
	unsigned long long q = 35;
	printf("%llu\n", eEAdS12(p, q, 22, x, y));

	printf("x: %lld\n", *x);
	printf("y: %lld\n", *y);
}

void primeTest() {

	unsigned long long n = 20;
	printf("Die %llu Primzahl lautet: %llu\n", n, prime1S12(n));

}

void primeRandom() {

	unsigned long long a = 10;
	unsigned long long b = 200;
	printf("Eine Zufallsprimzahl zwischen [%llu und %llu] lautet: %llu\n", a, b, primeZS12(a, b));

}

void primeRange() {

	unsigned long long n = 100;
	unsigned long long * array = malloc(sizeof(unsigned long long) * n);

	n = primeNS12(array, n);

	printf("Number of primes: %llu\n", n);

	int i;
	for(i = 0; i < n; i++) 
		printf("%llu, ", array[i]);
	

}

void modInv() {

	unsigned long long a = 3;
	unsigned long long n = 17;

	printf("mod inverse: %llu, %llu = %llu", a, n, ModInvS12(a, n));

}



/**
 *	Test application for cryptlib library
 *
 */
int main(int argc, char * argv[] ) {

	unsigned long long a = 45948;
	unsigned long long b = 32576;
	
	printf("ggT(%llu, %llu) = %llu\n", a, b, ggTS12(a, b));
	

	unsigned long long c = 85443;
	
	printf("phi(%llu) = %llu\n", c, EulPhiS12(c));

	
	unsigned long long d = 24877;
	unsigned long long e = 36491;

	long long x1 = 0;
	long long y1 = 0;
	
	long long * x = &x1;
	long long * y = &y1;


	printf("eEA(%llu, %llu) = %llu\n", d, e, eEAS12(d, e, x, y));


	unsigned long long a1 = 249;
	unsigned long long b1 = 237;
	unsigned long long n1 = 528;
	
	printf("%llu, %llu, %llu, %llu \n", a1, b1, n1, ModExpS12(a1, b1, n1));


	unsigned long long f = 416;
	unsigned long long g = 74371;

	printf("ModInv: %llu -1 mod %llu = %llu", f, g, ModInvS12(f, g));



	return 0;
}

